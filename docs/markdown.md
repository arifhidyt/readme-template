<a name="readme-top"></a>

<div align="center">
  <!-- You are encouraged to replace this logo with your own! Otherwise you can also remove it. -->
  <img src="aruna-logo.png" alt="logo" width="140"  height="auto" />
  <br/>

  <h1><b>Chapter Backend Empire</b></h1>

</div>

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [📖 About the Project](#about-project)
  - [🛠 Built With](#built-with)
    - [Tech Stack](#tech-stack)
- [🚀 API flow](#api-flow)
- [💻 Getting Started](#getting-started)
  - [Setup](#setup)
  - [Prerequisites](#prerequisites)
  - [Install](#install)
  - [Usage](#usage)
  - [Run tests](#run-tests)
- [👥 Authors](#authors)

<!-- PROJECT DESCRIPTION -->

# 📖 [Nepthune Service] <a name="about-project"></a>

> Describe your project in 1 or 2 sentences.

**[Nepthune service]** is a...

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

> Describe the tech stack and include only the relevant sections that apply to your project.

- PubSub
- Redis
- APM Log

<!-- API Flow -->

## 🚀 API Flow <a name="api-flow"></a>

> This flow diagram for every API rest of service.
```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```


```mermaid
    sequenceDiagram
    participant client as Client
    participant gw as API Gateway
    participant nepthune as Nepthune
    participant odoo as Odoo RPC

    Note over client,odoo: Create Customer
    client->>+gw: Call API [POST]<br/>[/api/v1/customer]
    gw->>+nepthune: Call API [POST]<br/>[/api/v1/customer]
    Note over nepthune: Mapping request
    nepthune->>+odoo: OdooRPC
    Note over odoo: model: res.partner<br/>method: create_customer
    odoo->>+nepthune: Send response
    Note over nepthune: Mapping response
    nepthune->>+gw: Send response
    gw->>+client: Send response
```

<details>
  <summary>Create Customer</summary>
  <ul>
    as
  </ul>
</details>

<details>
  <summary>Server</summary>
  <ul>
    <li><a href="https://expressjs.com/">Express.js</a></li>
  </ul>
</details>

<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.postgresql.org/">PostgreSQL</a></li>
  </ul>
</details>


<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

> Describe how a new developer could make use of your project.

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:
- golang version 1.19


### Setup

Clone this repository to your desired folder:
```sh
  git clone https://gitlab.com/aruna_core_system/chapter-backend/nepthune.git
```


### Download dependencies

Download dependencies this project with:
```go
  go mod tidy
```

### Usage

To run the project, execute the following command:
```sh
  go run cmd/Main.go
```

### Run tests

To run tests, run the following command:
```sh
  make gotest-dev
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- AUTHORS -->

## 👥 Authors <a name="authors"></a>

> Chapter Backend Team

<p align="right">(<a href="#readme-top">back to top</a>)</p>
